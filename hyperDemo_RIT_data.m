clear; close all; clc; dbstop if error;

%--------------------------------------------------------------------------
% This demo process the data from the RIT Target Detection Blind Test
% contest which is located at: http://dirsapps.cis.rit.edu/blindtest/
% To use this file, you select a target detection algorithm and a target to
% find and then the script runs the algorithm and outputs the data into the
% outputDir.  Two files are outputted, a .img and a .hdr. You upload these
% files to the RIT website and they are automatically scored.
%--------------------------------------------------------------------------
% Parameters
inputFilename = 'data\RIT HSI Data\blind_test\HyMap\blind_test_refl.img';
fasticaToolboxPath = '..\matlab_hyperspectral_toolbox\trunk\FastICA_25';
targetFilenames = {'data\RIT HSI Data\blind_test\SPL\F5\F5_l.txt'};
outputDir = 'RIT Data Results\\';
% See switch statement for algorithm choices
algorithm = 'ace'; 
%--------------------------------------------------------------------------

addpath(fasticaToolboxPath);
mkdir(outputDir);

% Read in the data
w = 280;
h = 800;
p = 126;
M = multibandread(inputFilename, [w h p], 'int16', 0, 'bil', 'ieee-le')/1e4;
lData = hyperGetHymapWavelengthsNm();

% Read in target signatures
[sig1, lSig] = hyperGetEnviSignature(targetFilenames{1});

% Get signature from data for comparison
fsig1 = squeeze(M(143,480,:));

% Resample data to commone wavelength set
desiredLambdas = lData;
sig1 = squeeze(hyperResample(sig1, lSig, desiredLambdas));
figure; plot(sig1); grid on; title('Signature 1');
    xlabel('Wavelength [nm]'); ylabel('Reflectance [%]');
    hold on; plot(fsig1, '--');
    legend('Recorded', 'From Image');
    
goodBands = 1:p;
    
% Display data
M = hyperConvert2d(M);
[M, H, snr] = hyperMnf(M, w, h);
M_pct = hyperPct(M, 3);
M_pct = hyperNormalize(hyperConvert3d(M_pct, w, h, 3));
figure; imagesc(M_pct); axis image; title('Scene');

% Data conditioning
M = M(goodBands, :);
sig1 = sig1(goodBands);
fsig1 = fsig1(goodBands);

%q = hyperHfcVd(M);
q = 53;

algorithm = lower(algorithm);
switch algorithm
    case 'ica-eea'
        [U, X] = hyperIcaEea(M, 50, sig1);
        r = X(1,:);
        r = hyperConvert3d(r, w, h, 1);
    case 'rx'
        r = hyperConvert3d(hyperRxDetector(M), w, h, 1);
    case 'matchedFilter'
        r = hyperConvert3d(hyperMatchedFilter(M, sig1), w, h, 1);
    case 'ace'
        r = hyperConvert3d(hyperAce(M, sig1), w, h, 1);
    case 'sid'
        r = hyperConvert3d(hyperSid(M, sig1), w, h, 1);
    case 'cem'
        r = hyperConvert3d(hyperCem(M, sig1), w, h, 1);
    case 'glrt'
        r = hyperConvert3d(hyperGlrt(M, sig1), w, h, 1);
    case 'osp'
        U = hyperAtgp(M, q, sig1);
        r = hyperConvert3d(hyperOsp(M, U, sig1), w, h, 1);
    case 'amsd'
        r = hyperConvert3d(hyperAmsd(M, U, sig1), w, h, 1);
    case 'hud'
        U = hyperAtgp(M, q, sig1);
        r = hyperConvert3d(hyperHud(M, U, sig1), w, h, 1);
    otherwise
        error('Incorrect algorithm name specified!\n');
end  

% Display results and write to file
figure; imagesc(r); axis image; colorbar; 
    title(algorithm);
r = (hyperNormalize(r)*2^10);
multibandwrite(r, sprintf('%s\\results.img', outputDir), 'bil', 'PRECISION', 'int16', 'MACHFMT', 'ieee-le');    

    
    